<?php

namespace App\Events;

use App\Models\Auth\User;
use App\Models\Tenant;
use App\Models\TenantInvitation;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserRegistered
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $tenant;

    public $user;

    public $invitation;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, Tenant $tenant, TenantInvitation $invitation = null)
    {
        $this->tenant = $tenant;
        $this->user = $user;
        $this->invitation = $invitation;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('user.register');
    }
}
