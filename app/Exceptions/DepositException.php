<?php

namespace App\Exceptions;

class DepositException extends \Exception
{
	public function __construct($message = "tss.deposit.unsufficient")
	{
		$this->message = trans($message);
	}
}