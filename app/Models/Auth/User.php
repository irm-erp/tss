<?php 

namespace App\Models\Auth;

use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable
{
    use \Zizaco\Entrust\Traits\EntrustUserTrait;

    protected $fillable = [
    	"name", "email", "password", "tenant_code", "access_token", "refresh_token",
    ];
}