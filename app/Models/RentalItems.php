<?php

namespace App\Models;

class RentalItems extends Model
{
    protected $table = "rental_items";

    public $timestamps = false;

    protected $fillable = [
        "rental_id",
        "app_code",
        "month_qty",
        "start_date",
        "finish_date",
        "sub_total",
    ];

    public static $rules = [
        "rental_id" => "required",
        "app_code" => "required|max:5",
        "month_qty" => "required",
        "start_date" => "date",
        "finish_date" => "date",
        "sub_total" => "required|numeric",
    ];

    public function attributeLabels()
    {
        return [
            "id" => "ID",
            "rental_id" => "Rental ID",
            "app_code" => "Application",
            "month_qty" => "Month Qty",
            "start_date" => "Start Date",
            "finish_date" => "Finish Date",
            "sub_total" => "Sub Total",
        ];
    }
}
