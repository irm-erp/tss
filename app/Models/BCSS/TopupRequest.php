<?php

namespace App\Models\BCSS;

use App\Models\Model;
use DB;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;

class TopupRequest extends Model
{
    protected $connection = "bcss";

    protected $table = "topup_request";

    protected $fillable = ["tenant_code", "ref_no", "amount", "img"];

    public function defaultValue()
    {
        return [
            "tenant_code" => Auth::user()->tenant_code,
        ];
    }

    public function rules()
    {
        return [
            "tenant_code" => "required|string|max:8",
            "ref_no" => "required|string|max:50",
            "amount" => "required|numeric",
            "img" => "required|image",
        ];
    } 

    public function attributeLabels()
    {
        return [
            "tenant_code" => "Tenant",
            "ref_no" => "Nomor Bukti",
            "amount" => "Deposit",
            "img" => "Image",
        ];
    }

    public function getImgAttribute()
    {
        $file = storage_path("app/public/topup/$this->id.jpg");

        if (!file_exists($file))
            return;

        return file_get_contents($file);
    }

    protected function performSave(array $options) 
    {
        $img = $this->attributes["img"];
        unset($this->attributes["img"]);

        $return = parent::performSave($options);

        // Save Image File
        if (isset($img) && $img instanceof UploadedFile) {
            $img->move(storage_path("app/public/topup"), $this->getKey().".jpg");
        }

        return $return;
    }
}