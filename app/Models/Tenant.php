<?php

namespace App\Models;

use App\Exceptions\DepositException;
use Auth;
use DB;

class Tenant extends Model
{
    protected $table = "tenant";

    protected $primaryKey = "code";

    public $incrementing = false;

    public $timestamps = false;

    public static $tenant;

    protected $fillable = [
        "name",
        "address",
        "city",
        "comm_contact_mail",
        "comm_contact_name",
        "comm_contact_phone",
        "claimable_deposit",
        "unclaimable_deposit",
        "fullname",
        "join_date",
        "tech_contact_mail",
        "tech_contact_name",
        "tech_contact_phone",
        "user_license",
        "parent_code",
    ];

    protected $attributes = [
        "claimable_deposit" => 0,
        "unclaimable_deposit" => 0,
        "user_license" => 1,
    ];

    public function rules() 
    {
        return [
            "name" => "required|string|min:3|max:16|unique:tenant",
            "fullname" => "required|string|max:50",
            "city" => "nullable|string|max:50",
            "address" => "nullable|string|max:255",
            "claimable_deposit" => "required|numeric",
            "unclaimable_deposit" => "required|numeric",
            "join_date" => "required|date",
            "comm_contact_mail" => "nullable|string|email|max:50",
            "comm_contact_name" => "nullable|string|max:50",
            "comm_contact_phone" => "nullable|numeric",
            "tech_contact_mail" => "nullable|string|email|max:50",
            "tech_contact_name" => "nullable|string|max:50",
            "tech_contact_phone" => "nullable|numeric",
            "user_license" => "required|integer",
        ];
    }

    public function attributeLabels()
    {
        return [
            "code" => "Code",
            "name" => "Name",
            "fullname" => "Company Name",
            "city" => "City",
            "address" => "Address",
            "claimable_deposit" => "Claimable Deposit",
            "unclaimable_deposit" => "Unclaimable Deposit",
            "join_date" => "Join Date",
            "comm_contact_mail" => "Email",
            "comm_contact_name" => "Name",
            "comm_contact_phone" => "Phone",
            "tech_contact_mail" => "Email",
            "tech_contact_name" => "Name",
            "tech_contact_phone" => "Phone",
            "user_license" => "User License",
        ];
    }

    public function generateCode()
    {
        $code = Tenant::max("code");
        $last_period = substr($code, 0, 4);
        $period = date("ym");

        if (empty($code) || $last_period != $period) {
            $count = 1;
        }
        else {
            $count = substr($code, 4) + 1;
        }

        return $period.str_pad($count, 4, "0", STR_PAD_LEFT);
    }

    public function beforeValidate()
    {
        if (!isset($this->code))
            $this->code = $this->generateCode();

        if (!isset($this->join_date))
            $this->join_date = date("Y-m-d");
    }

    public static function user()
    {
        if (self::$tenant == null) {
            self::$tenant = self::findOrFail(Auth::user()->tenant_code);
        }

        return self::$tenant;
    }

    public function getDepositAttribute()
    {
        return $this->claimable_deposit + $this->unclaimable_deposit;
    }

    public function subDeposit($value)
    {
        // deposit tidak mencukupi
        if ($this->deposit < $value) {
            throw new DepositException(trans("tss.deposit.unsufficient"));
        }

        if ($this->unclaimable_deposit < $value) {
            if ($this->unclaimable_deposit > 0) {
                $value -= $this->unclaimable_deposit;
                $this->unclaimable_deposit = 0;
            }

            // Claimable Deposit
            $this->claimable_deposit -= $value;

            // Apply Commission on Upline
            if (!empty($this->parent_code)) {
                $commission = $value * config("tss.commission.rental");
                self::findOrFail($this->parent_code)->applyCommission($commission);
            }
        }
        // Unclaimable Deposit
        else $this->unclaimable_deposit -= $value;

        $this->save();
    }

    public function applyCommission($commission, $description = null)
    {
        $this->unclaimable_deposit += $commission;

        DepTransaction::logCommission(
            $commission, 
            $description, 
            ["tenant_code" => $this->code]
        );

        return $this->save();
    }

    /** Query Scopes */

    public function scopeAuth($query)
    {
        return $query->where("tenant.code", Auth::user()->tenant_code);
    }

    public function scopeDownlines($query, $code = null)
    {
        if ($code == null) {
            $code = Auth::user()->tenant_code;
        }

        return $query->where("tenant.parent_code", $code);
    }

    public function scopeWithUser($query)
    {
        return $query->join("users as u", "u.tenant_code", "=", "tenant.code");
    }
}
