<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class Application extends Model
{
    protected $table = "application";

    protected $primaryKey = "code";

    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = [
        "description",
        "name",
        "tarif",
    ];

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('withoutUserLicense', function(Builder $builder) {
            $builder->where("code", "!=", "00");
        });
    }

    public function rules()
    {
        return [
            "description" => "required|max:255",
            "name" => "required|max:50",
            "tarif" => "required|numeric",
        ];
    }

    public function attributeLabels()
    {
        return [
            "code" => "Code",
            "description" => "Description",
            "name" => "Name",
            "tarif" => "Tarif",
        ];
    }

    public function getImageAttribute()
    {
        return app_image($this->code);
    }

    public function scopeUserLicense($query)
    {
        return $query->withoutGlobalScope("withoutUserLicense")->where("code", "00");
    }
}
