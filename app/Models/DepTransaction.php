<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;

class DepTransaction extends Model
{
    const MODE_TOPUP = "1";
    const MODE_COMMISSION = "2";
    const MODE_RENTAL = "3";
    const MODE_USER_LICENSE = "4";

    protected $table = "dep_transaction";

    public $timestamps = false;

    protected $fillable = [
        "description",
        "is_debet",
        "ref_no",
        "tenant_code",
        "trans_date",
        "trans_mode",
        "balance",
    ];

    public function defaultValue()
    {
        return [
            "tenant_code" => Auth::user()->tenant_code,
            "trans_date" => date("Y/m/d H:i:s"),
        ];
    }

    public function rules() 
    {
        return [
            "description" => "max:255",
            "is_debet" => "required",
            "ref_no" => "required|max:20",
            "tenant_code" => "required|max:5",
            "trans_date" => "required",
        ];
    }

    public function attributeLabels()
    {
        return [
            "id" => "ID",
            "is_debet" => "Is Debet",
            "ref_no" => "Ref No",
            "tenant_code" => "Tenant Code",
            "trans_date" => "Date",
            "trans_mode" => "Trans Mode",
            "description" => "Description",
        ];
    }

    public static function __callStatic($method, $args)
    {
        if (preg_match_all("/log(.+)/", $method, $match)) {
            $name = current($args);
            $description = next($args);
            $options = next($args);

            $model = new DepTransaction([
                "balance" => $name,
                "description" => $description,
                "ref_no" => $options["ref_no"],
            ]);

            // more attributes
            $overrides = next($args);

            if (!empty($overrides)) {
                $model->fill($overrides);
            }

            switch (strtolower($match[1][0])) {
                case "topup":
                    $model->trans_mode = self::MODE_TOPUP;
                    $model->is_debet = true;
                    break;

                case "commission":
                    $model->trans_mode = self::MODE_COMMISSION;
                    $model->is_debet = true;
                    break;

                case "rental":
                    $model->trans_mode = self::MODE_RENTAL;
                    $model->is_debet = false;
                    break;

                case "userlicense":
                    $model->trans_mode = self::MODE_USER_LICENSE;
                    $model->is_debet = false;
                    break;
            }

            // set default description if empty
            if (empty($model->description)) {
                $model->description = title_case($match[1][0]);
            }

            $model->save();
            return;
        }

        return parent::__callStatic($method, $args);
    }
}
