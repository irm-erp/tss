<?php

namespace App\Models;

class Features extends Model
{
    protected $table = "features";

    protected $primaryKey = "code";

    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = [
        "app_code",
        "description",
        "name",
    ];

    public static $rules = [
        "app_code" => "required|max:4",
        "description" => "max:255",
        "name" => "required|max:50",
    ];

    public function attributeLabels()
    {
        return [
            "app_code" => "App Code",
            "code" => "Code",
            "description" => "Description",
            "name" => "Name",
        ];
    }
}
