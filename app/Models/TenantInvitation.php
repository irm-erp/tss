<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;

class TenantInvitation extends Model
{
    protected $table = "tenant_invitation";

    protected $fillable = ["tenant_code", "receiver_mail", "token", "is_accepted"];

    public function defaultValue()
    {
        return ["is_accepted" => false];
    }

    public function rules() 
    {
        return [
            "tenant_code" => "required|max:8",
            "receiver_mail" => "required|email",
            "token" => "required|string",
            "is_accepted" => "boolean",
        ];
    }

    public function attributeLabels()
    {
        return [
            "tenant_code" => "Tenant",
            "receiver_mail" => "Receiver",
            "token" => "Token",
            "is_accepted" => "Accepted",
        ];
    }

    public function beforeSave()
    {
        if (!isset($this->token)) {
            $this->token = $this->generateToken();
        }
    }

    public function generateToken()
    {
        return uniqid(Auth::user()->id, true);
    }

    public function accept($token)
    {
        if ($token == $this->token) {
            // update accepted status
            $this->update(["is_accepted" => true]);

            return true;
        }

        return false;
    }

    public function scopeAccepted($query, $email = null)
    {
        $query->where("is_accepted", true);

        if ($email != null) {
            $query->where("receiver_mail", $email);
        }

        return $query;
    }
}
