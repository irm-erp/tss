<?php

namespace App\Models;

use Auth;

class Rental extends Model
{
    const STATUS_DRAFT = 0;
    const STATUS_PAID = 1;
    const STATUS_INSTALLED = 2;

    protected $table = "rental";

    public $timestamps = false;

    protected $fillable = [
        "contract_no",
        "req_date",
        "qty",
        "total",
        "status",
        "tenant_code",
        "app_code",
    ];

    protected $attributes = [
        "status" => 0,
        "total" => 0,
    ];

    public function rules() 
    {
        return [
            "contract_no" => "required|string",
            "req_date" => "required|date",
            "qty" => "required|integer",
            "total" => "required|numeric",
            "status" => "required|string|max:1",
            "tenant_code" => "required|string|max:8",
            "app_code" => "required|string",

            "attr" => [
                "status" => [
                    self::STATUS_DRAFT => "Draft",
                    self::STATUS_PAID => "Paid",
                    self::STATUS_INSTALLED => "Installed",
                ],
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            "id" => "ID",
            "contract_no" => "Contract Number",
            "req_date" => "Request Date",
            "qty" => "Qty",
            "total" => "Total",
            "status" => "Status",
            "tenant_code" => "Tenant",
            "app_code" => "Application",
        ];
    }

    public function beforeValidate()
    {
        if (!isset($this->contract_no)) {
            $this->contract_no = $this->getContractNoAttribute();
        }

        if (!isset($this->req_date)) {
            $this->req_date = date("Y-m-d");
        }
    }

    public function getContractNoAttribute()
    {
        if (!isset($this->attributes["contract_no"])) {
            $this->attributes["contract_no"] = code_increment(date("ym"), Rental::max("contract_no"));
        }

        return $this->attributes["contract_no"];
    }

    public function scopeWithApp($query)
    {
        return $query->join("application as a", "a.code", "=", "rental.app_code");
    }

    public function scopeAuth($query)
    {
        return $query->where("tenant_code", Auth::user()->tenant_code);
    }
}
