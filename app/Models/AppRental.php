<?php

namespace App\Models;

use Auth;

class AppRental extends Model
{
    const STATUS_INACTIVE = '0';
    const STATUS_ACTIVE = '1';
    const STATUS_TERMINATED = '2';

    protected $table = "app_rental";

    public $timestamps = false;

    protected $fillable = [
        "tenant_code",
        "app_code",
        "start_date",
        "finish_date",
        "status",
    ];

    public static $rules = [
        "start_date" => "required|date",
        "finish_date" => "required|date",
        "status" => "required|string|max:1",
        "tenant_code" => "required|string|max:8",
        "app_code" => "required|string",
    ];

    protected $attributes = [
        "status" => self::STATUS_INACTIVE,
    ];

    public function attributeLabels()
    {
        return [
            "id" => "ID",
            "tenant_code" => "Tenant",
            "app_code" => "Application",
            "start_date" => "Start Date",
            "finish_date" => "Finish Date",
            "status" => "Status",

            "attr" => [
                "status" => [
                    self::STATUS_INACTIVE => "Non Active",
                    self::STATUS_ACTIVE => "Active",
                    self::STATUS_TERMINATED => "Terminated",
                ],
            ],
        ];
    }

    public function scopeWithApp($query)
    {
        return $query->join("application as a", "a.code", "=", "app_rental.app_code");
    }

    public function scopeAuth($query)
    {
        return $query->where("tenant_code", Auth::user()->tenant_code);
    }
}
