<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use App\Models\DepTransaction;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class InvitationCommission
{
    protected $commission;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->commission = config("tss.commission.invitation");
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        if ($event->invitation == null) {
            return;
        }

        $event->tenant->applyCommission(
            $this->commission, 
            "Komisi undangan tenant ({$event->tenant->fullname})"
        );
    }
}
