<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Mail\InviteTenant;
use App\Models\Tenant;
use App\Models\TenantInvitation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Laraden\Facades\Flash;
use Yajra\Datatables\Facades\Datatables;

class AgentController extends Controller
{
    public function downlines()
    {
        return Datatables::of(
            Tenant::downlines()
                ->withUser()
                ->select("tenant.*", "u.name", "u.email")
        )->make(true);
    }

    public function invite(Request $req)
    {
        try {
            DB::beginTransaction();
            $user = $req->user();

            $invitation = TenantInvitation::createOrFail([
                "tenant_code" => $user->tenant_code, 
                "receiver_mail" => $req->email, 
            ]);

            Mail::to($req->email)->send(new InviteTenant($invitation));
            DB::commit();
            Flash::success(trans("rental.request.sent"));
        }
        catch (\Exception $e) {
            DB::rollback();
            Flash::danger("Failed to invite '$req->email'");
            throw $e;
        }

        return back();
    }
}
