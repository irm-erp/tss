<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\AppRental;
use App\Models\Application;
use Auth;
use Illuminate\Http\Request;

class JsonController extends Controller
{
    public function handle(Request $req, $name)
    {
        $getter = "get".camel_case($name);

        if (method_exists($this, $getter)) {
            return $this->{$getter}($req);
        }
        else abort(404);
    }

    public function getApplication($req)
    {
        $data = Application::get();

        foreach ($data as $app) {
            $app->image = $app->image;
        }

        return $data;
    }

    public function getLang($req)
    {
        if (!$req->has("q"))
            return;
        
        return trans($req->q);
    }

    public function getAppRental($req)
    {
        $data = AppRental::auth()->withApp()->get();

        foreach ($data as $app) {
            $app->image = app_image($app->code);
        }

        return $data;
    }
}
