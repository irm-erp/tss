<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StorageController extends Controller
{
	private $dir;

	public function __construct()
	{
		$this->dir = storage_path("app/public");
	}

	public function file(Request $req)
	{
		if ($req->has("path")) {
			$file = $this->dir."/".$req->input("path");

			if (file_exists($file)) {
				return response()->file($file);
			}
		}
		
		abort(404);
	}

	public function image(Request $req)
	{
		if ($req->has("path")) {
			$file = $this->dir."/".$req->input("path");

			if (file_exists($file)) {
				return response()->file($file);
			}
		}
		
		abort(404);
	}
}