<?php

namespace App\Http\Controllers;

use App\Models\Rental;
use App\Models\Tenant;
use App\Models\TenantInvitation;
use App\Models\User;
use Auth;
use DB;
use Datatables;
use Flash;
use Hash;
use Illuminate\Http\Request;
use Mail;
use Notif;
use Validator;

class TenantController extends \App\Http\Controllers\Controller
{
    public function profile(Request $req)
    {
        // Update Profile
        if ($req->isMethod("post")) {
            Tenant::user()->updateOrFail($req->all());
            Notif::success("Profil disimpan");

            return back();
        }

        return view("tenant.profile", [
            "model" => Tenant::user(),
        ]);
    }

    public function password(Request $req)
    {
        if ($req->isMethod("post")) {
            $user = Auth::user();

            // Wrong Password
            if (!Hash::check($req->current_password, $user->password)) {
                Notif::error("Password Salah!");
                return back();
            }

            // Password confirmation failed
            if ($req->new_password != $req->confirm_password) {
                Notif::error("Password Tidak Sama!");
                return back();
            }

            // Update password
            $user->password = bcrypt($req->new_password);
            $user->save();

            Notif::success("Password anda telah diganti.");

            return back();
        }

        return view("tenant.password");
    }

    public function verify($tenant_code, $code)
    {
        $user = User::where("tenant_code", $tenant_code)->firstOrFail();

        if ($user->email_verified != true) {
            // Email Verified
            if (Hash::check($tenant_code, base64_decode($code))) {
                $user->email_verified = true;
                $user->save();
            }

            Flash::success(
                "<h4>Terima Kasih</h4> 
                Email anda telah terverifikasi"
            );
        }

        return redirect("login");
    }

    public function acceptInvitation($invitation_id, $token)
    {
        $invitation = TenantInvitation::findOrFail($invitation_id);

        // The Receiver had had an account
        if (User::where("email", $invitation->receiver_mail)->count() > 0) {
            // redirect to login page
            $res = redirect("login");
        }
        // The Receiver had already accepted the Invitation
        else if ($invitation->accepted == true
            || TenantInvitation::accepted($invitation->receiver_mail)->count() > 0) {
            Flash::info(
                "Anda sudah pernah melakukan konfirmasi undangan
                <br> Silahkan lakukan registrasi untuk account anda"
            );
        
            $res = redirect("register");
        }
        // Accept the invitation
        else {
            $invitation->accept($token);

            Flash::success(
                "<h4>Email anda sudah terverifikasi</h4>
                Silahkan lakukan registrasi untuk account anda."
            );
            
            $res = redirect("register");
        }

        return $res->withInput([
            "email" => $invitation->receiver_mail,
        ]);
    }
}
