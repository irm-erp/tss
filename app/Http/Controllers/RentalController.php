<?php

namespace App\Http\Controllers;

use App\Models\AppRental;
use App\Models\Application;
use App\Models\DepTransaction;
use App\Models\Rental;
use App\Models\RentalItems;
use App\Models\Tenant;
use DB;
use Datatables;
use Flash;
use Html;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RentalController extends \App\Http\Controllers\Controller
{
    public function request(Request $req)
    {
        $model = new Rental();

        try {
            DB::beginTransaction();

            $tenant = Tenant::user();
            $binding = [$tenant->code, $model->contract_no, Rental::STATUS_PAID];

            foreach ($req->app as $code => $qty) {
                $binding[] = $code;
                $binding[] = intval($qty);
            }

            $values = array_fill(0, count($req->app), "(?, ?)");

            DB::insert(
                "INSERT INTO rental (tenant_code, contract_no, status, req_date, qty, total, app_code)
                SELECT ?, ?, ?, NOW(), t.qty::int, a.tarif*t.qty::int, t.app_code 
                FROM (VALUES ".implode(",", $values).") AS t(app_code, qty)
                INNER JOIN application a ON a.code=t.app_code", $binding
            );

            // Sedot Deposit
            $total = Rental::where("contract_no", $model->contract_no)->sum("total");
            $tenant->subDeposit($total);

            // Install apps
            $this->install();

            // Log Transaction
            DepTransaction::logRental(
                $total, 
                "Application Rental", 
                ["ref_no" => $model->contract_no]
            );

            Flash::success(trans("rental.request.sent"));
            DB::commit();
        }
        catch (\Exception $e) {
            DB::rollback();
            Flash::danger($e->getMessage());
            // Flash::danger(trans("notif.failed"));
            // throw $e;
        }

        return redirect("dashboard");
    }

    protected function install()
    {
        // create / extend app rental
        DB::insert(
            "INSERT INTO app_rental AS ar (tenant_code, app_code, start_date, finish_date, status)
            SELECT tenant_code, app_code, now(), now() + CAST(SUM(qty)||' months' AS interval), ? FROM rental 
            WHERE status=? AND tenant_code=?
            GROUP BY tenant_code, app_code
            ON CONFLICT (tenant_code, app_code) DO UPDATE 
            SET finish_date = ar.finish_date + (excluded.finish_date-now())", 
            [AppRental::STATUS_ACTIVE, Rental::STATUS_PAID, Auth::user()->tenant_code]
        );

        // update rental status to installed
        Rental::where("status", Rental::STATUS_PAID)
            ->where("req_date", "<=", DB::raw("NOW()"))
            ->update(["status" => Rental::STATUS_INSTALLED]);
    }

    public function userLicense(Request $req)
    {
        $user_license = Application::userLicense()->first();

        if ($req->isMethod("post")) {
            try {
                DB::beginTransaction();
                $tenant = Tenant::user();

                // Make Transaction History
                $total = $user_license->tarif * $req->qty;
                Rental::createOrFail([
                    "tenant_code" => $tenant->code,
                    "status" => Rental::STATUS_INSTALLED,
                    "qty" => $req->qty,
                    "total" => $total,
                    "app_code" => $user_license->code,
                ]);

                // Sedot Deposit
                $tenant->subDeposit($total);

                // Add User License
                $tenant->user_license += $req->qty;
                $tenant->save();

                // Log Transaction
                DepTransaction::logUserLicense($total, "@{$req->qty} User License");

                Flash::success(trans("rental.user_license.bought"));
                DB::commit();
            }
            catch (\Exception $e) {
                Flash::danger($e->getMessage());
                // Flash::danger(trans("notif.failed"));
                DB::rollback();
                throw $e;
            }

            return back();
        }

        return view("rental.user-license", [
            "price" => $user_license->tarif,
        ]);
    }

    public function status($code)
    {
        $rental = AppRental::where("app_code", $code)->firstOrFail();

        if ($rental->status==AppRental::STATUS_ACTIVE) {
            $rental->status = AppRental::STATUS_INACTIVE;
        }
        else if ($rental->status==AppRental::STATUS_INACTIVE) {
            $rental->status = AppRental::STATUS_ACTIVE;
        }

        $rental->saveOrFail();

        return back();
    }
}
