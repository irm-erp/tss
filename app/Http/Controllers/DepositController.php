<?php

namespace App\Http\Controllers;

use App\Models\BCSS\TopupRequest;
use App\Models\DepTransaction;
use App\Models\Tenant;
use App\Models\TenantInvitation;
use Auth;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laraden\Facades\Flash;

class DepositController extends \App\Http\Controllers\Controller
{
    public function index()
    {
        return view([
            "model" => new DepTransaction,
            "tenant" => Tenant::user(),
        ]);
    }

    public function topup(Request $req)
    {
        if ($req->isMethod("post")) {
            try {
                DB::beginTransaction();

                TopupRequest::createOrFail($req->all());
                DepTransaction::logTopup(
                    $req->amount, 
                    "Topup Request", 
                    ["ref_no" => $req->ref_no]
                );

                DB::commit();
                Flash::success(trans("rental.request.sent"));
            }
            catch (\Exception $e) {
                DB::rollback();
                throw $e;
            }

            return redirect("deposit");
        }

        return view("deposit.topup");
    }

    public function history()
    {
        $res = Datatables::of(DepTransaction::query())->make(true);

        return response_map($res, function($item) {
            $item->trans_date = date("d M Y", strtotime($item->trans_date));

            if ($item->is_debet) {
                $item->debet = currency($item->balance);
                $item->kredit = "";
            }
            else {
                $item->debet = "";
                $item->kredit = currency($item->balance);
            } 

            return $item;
        });
    }
}
