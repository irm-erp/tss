<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Application;
use App\Models\Tenant;
use Auth;
use Illuminate\Http\Request;

class AppController extends Controller
{
    public function index()
    {
        if (Auth::check()) {
            return view("app.store");
        }
        else {
            return view("app.index", [
                "list" => Application::orderBy("name")->paginate(20),
            ]);
        }
    }

    public function show($id)
    {
    	return view("app.show", [
            "model" => Application::findOrFail($id),
        ]);
    }

    public function dashboard()
    {
        return view("app.dashboard", [
            "tenant" => Tenant::user(),
        ]);
    }
}
