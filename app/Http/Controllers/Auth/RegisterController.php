<?php

namespace App\Http\Controllers\Auth;

use App\Events\UserRegistered;
use App\Http\Controllers\Controller;
use App\Models\Auth\User;
use App\Models\Tenant;
use App\Models\TenantInvitation;
use DB;
use Flash;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255|unique:tenant',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'company_name' => 'required|string|max:255',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        try {
            DB::beginTransaction();
            $invitation = TenantInvitation::accepted($data["email"])->first();
            $is_invited = $invitation!==null;

            // Create Tenant
            $tenant = Tenant::createOrFail([
                "name" => $data["name"],
                "fullname" => $data["company_name"],
                "parent_code" => $is_invited ? $invitation->tenant_code : null,
            ]);

            // Create User Login
            $user = User::create([
                "name" => $data["name"],
                "email" => $data["email"],
                "password" => bcrypt($data["password"]),
                "tenant_code" => $tenant->code,
                "email_verified" => $is_invited,
            ]);


            if ($is_invited) {
                event(new UserRegistered($user, $tenant, $invitation));
            }
            // Send Verification Email
            else {
                Mail::send("auth.emails.verify", [
                    "tenant_code" => $tenant->code,
                    "code" => base64_encode(bcrypt($tenant->code)),
                ], function($msg) use ($data) {
                    $msg->from(config("mail.username"), config("mail.from.name"));
                    $msg->to($data["email"])->subject("[ERP] Email Verification");
                });
                
                Flash::success(
                    "<b>Registrasi telah berhasil</b><br>
                    Silahkan periksa email anda untuk verifikasi."
                );
            }

            DB::commit();

            return $user;
        }
        catch (\Exception $e) {
            DB::rollback();
            Flash::danger($e->getMessage());
            throw $e;
        }
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        // $this->guard()->login($user);

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }
}
