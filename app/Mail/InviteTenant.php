<?php

namespace App\Mail;

use App\Models\Auth\User;
use App\Models\Tenant;
use App\Models\TenantInvitation;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InviteTenant extends Mailable
{
    use Queueable, SerializesModels;

    public $sender;

    public $tenant;

    public $invitation;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(TenantInvitation $invitation)
    {
        $this->invitation = $invitation;
        $this->tenant = Tenant::findOrFail($this->invitation->tenant_code);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown("emails.agent.invite");
    }
}
