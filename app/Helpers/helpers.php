<?php

function app_image($code)
{
	$file = "img/application/$code.png";
        
    return file_exists(public_path($file)) ? $file : "img/application/default.png";
}

function platform_url($url = null)
{
	return str_replace("{tenant}", Auth::user()->name, config("platform.domain")).$url;
}
