<table class="table table-bordered" datatable="{{ url("deposit/history") }}">
  <thead>
    <tr>
      <th dt-field="trans_date"> {{ $model->label("trans_date") }} </th>
      <th dt-field="debet" sort="false"> {{ $model->label("debet") }} </th>
      <th dt-field="kredit" sort="false"> {{ $model->label("kredit") }} </th>
      <th dt-field="ref_no"> {{ $model->label("ref_no") }} </th>
      <th dt-field="description"> {{ $model->label("description") }} </th>
    </tr>
  </thead>
</table>