<div class="form-group row">
	<div class="col-md-offset-3 col-md-6">
		<table class="table">
			<colgroup>
				<col width="50%" />
			</colgroup>

			<tr>
				<td class="text-right">Claimable</td>
				<td>{{ currency($tenant->claimable_deposit) }}</td>
			</tr>
			<tr>
				<td class="text-right">Unclaimable</td>
				<td>{{ currency($tenant->unclaimable_deposit) }}</td>
			</tr>
			<tr>
				<th class="text-right">Total</th>
				<th>{{ currency($tenant->deposit) }}</th>
			</tr>
		</table>
	</div>
</div>

<div class="row text-center">
  <a href="{{ url("deposit/topup") }}" class="btn btn-primary" modal>
    <i class="fa fa-arrow-up"></i> Top Up
  </a>
</div>
