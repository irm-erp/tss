@extends("app")

@section("title", "Deposit")

@section("content")
	{!! Form::open(["enctype" => "multipart/form-data"]) !!}
		{!! Form::group("img", "img", ["label" => "Bukti Pembayaran"]) !!}
		{!! Form::group("text", "ref_no") !!}
		{!! Form::group("text", "amount") !!}
		<div class="form-group">
			<div class="col-md-offset-3 col-md-9">
				<button type="submit" class="btn btn-primary"> @lang("action.send") </button>
			</div>
		</div>
	{!! Form::close() !!}
@endsection
