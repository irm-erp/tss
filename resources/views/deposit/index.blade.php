@extends("app")

@section("title", "Deposit")

@section("content")
	@include("app.dashboard-header")

	<div class="tabs">
	  <ul class="nav nav-tabs">
      <li class="active">
        <a href="#summary" data-toggle="tab">
          <i class="fa fa-history"></i> Summary
        </a>
      </li>

      <li> 
        <a href="#downline" data-toggle="tab"> 
          <i class="fa fa-sitemap"></i> Downline 
        </a> 
      </li>
      
      <li>
        <a href="#history" data-toggle="tab">
          <i class="fa fa-history"></i> Transaction History
        </a>
      </li>
	  </ul>

	  <div class="tab-content">
      <div id="summary" class="tab-pane active">
        @include("deposit.summary", compact("model", "tenant"))
      </div>

      <div id="history" class="tab-pane">
        @include("deposit.history", compact("model", "tenant"))
      </div>

      <div id="downline" class="tab-pane">
        @include("agent.downline", compact("model", "tenant"))
      </div>
	  </div>
	</div>
@endsection

@push("style")
	<style type="text/css"> 
		hr { margin: 0px 0px 20px !important }
	</style>
@endpush
