@extends("auth.app")

@section("title", "Sign Up")

@section("content-auth")
<form class="form-horizontal" role="form" method="POST" action="{{ url('register') }}">
    {!! csrf_field() !!}

    @foreach([
        "email" => "Email",
        "name" => "Username",
        "company_name" => "Company Name",
    ] as $name => $label)
        <div class="form-group mb-lg {{ $errors->has($name) ? 'has-error' : '' }}">
            <label>{{ $label }}</label>
            <input name="{{ $name }}" type="text" class="form-control input-lg" 
                value="{{ old($name) }}" />

            @if ($errors->has($name))
                <span class="help-block">
                    <strong>{{ $errors->first($name) }}</strong>
                </span>
            @endif
        </div>
    @endforeach

    <div class="row">
        <div class="form-group col-md-6 {{ $errors->has('password') ? 'has-error' : '' }}">
            <label>Password</label>
            <input name="password" type="password" class="form-control input-lg">

            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group col-md-6 pull-right{{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
            <label>Confirm Password</label>
            <input name="password_confirmation" type="password" class="form-control input-lg">

            @if ($errors->has('password_confirmation'))
                <span class="help-block">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group mb-lg {{ $errors->has('captcha') ? 'has-error' : '' }}">
        <label>Captcha</label>
        <div class="input-group input-group-icon">
            <span class="input-group-addon" style="padding: 0px 5px">
                {!! captcha_img() !!}
            </span>
            <input name="captcha" type="text" class="form-control input-lg" />
        </div>

        @if ($errors->has('captcha'))
            <span class="help-block">
                <strong>{{ $errors->first('captcha') }}</strong>
            </span>
        @endif
    </div>

    <div class="row text-right">
        <button type="submit" class="btn btn-primary">Sign Up</button>
    </div>
</form>
@endsection