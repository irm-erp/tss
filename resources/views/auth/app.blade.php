@extends("app")

@section("content")
	<div class="row">
		<div class="panel panel-default col-md-6 col-md-offset-3">
			<div class="panel-body">
				<div class="row">
					<h3 class="page-header mt-sm mb-xlg">@yield("title")</h3>
				</div>
				
				@yield("content-auth")
			</div>
		</div>
	</div>
@endsection