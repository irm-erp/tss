<?php

use App\Models\AppRental;

?>

@extends("app")

@section("title", "Dashboard")

@section("content")
  @include("app.dashboard-header")

  {{-- Header --}}
  <div class="row">
    {{-- Statuses --}}
    <div class="col-xs-6">
      <div class="input-group">
        <span class="input-group-addon">
          <b class="mr-xs" tooltip="Applications"> 
            <i class="fa fa-rocket"></i> @{{ app_count.installed }}
          </b>
        </span>

        <span class="input-group-addon">
          <b class="mr-xs text-success" tooltip="Active"> 
            <i class="fa fa-check"></i> @{{ app_count.active }}
          </b>
          <b class="mr-xs text-muted" tooltip="Inactive"> 
            <i class="fa fa-power-off"></i> @{{ app_count.inactive }}
          </b>
          <b class="mr-xs text-danger" tooltip="Terminated"> 
            <i class="fa fa-remove"></i> @{{ app_count.terminated }}
          </b>
        </span>

        <span class="input-group-addon">
          <b class="mr-xs" tooltip="User License"> 
            <i class="fa fa-user"></i> {{ intval($tenant->user_license) }}
          </b>
          <a href="{{ url("rental/user-license") }}" class="btn btn-xs btn-default" modal-sm>
            <i class="fa fa-plus"></i>
          </a>
        </span>
      </div>
    </div>

    {{-- Deposit --}}
    <div class="mt-md col-xs-6 text-right">
      <i class="fa fa-money"></i> 
      <b>{{ currency(Tenant::user()->deposit) }}</b> 
    </div>

    {{-- <div class="col-xs-6 text-right">
      <div class="dropdown">
        <div class="dropdown-toggle" data-toggle="dropdown">
          <i class="fa fa-money"></i> 
          <b>{{ currency(Tenant::user()->deposit) }}</b> 
          <i class="fa fa-caret-down"></i>
        </div>
        <ul class="dropdown-menu">
          <li>
            <a href="{{ url("deposit") }}">
              <i class="fa fa-eye"></i> Review
            </a>
          </li>
          <li>
            <a href="{{ url("deposit/topup") }}" modal>
              <i class="fa fa-arrow-up"></i> Top Up
            </a>
          </li>
        </ul>
      </div>
    </div> --}}
  </div>
  <hr class="mt-sm">

  {{-- Empty Rental --}}
  <div v-if="applications.length == 0" class="jumbotron bg-none text-center">
    <p class="text-muted">Anda belum menyewa aplikasi</p>

    <a href="{{ url("/") }}" class="btn btn-lg btn-primary">
      <i class="fa fa-shopping-basket"></i> Store
    </a>
  </div>

  {{-- Applications --}}
  <div v-for="app in applications" class="media thumbnail p-md">
    <div class="media-left media-middle">
      <img class="media-object" height="60px" :alt="app.name" :src="fn.url(app.image)" />
    </div>
    <div class="media-body pt-none">
      <b class="media-heading text-md mt-xs">@{{ app.name }}</b>

      <div class="row">
        <div class="col-md-9">
          <span v-if="app.status=={{ AppRental::STATUS_ACTIVE }}" class="label label-success">
            <i class="fa fa-check"></i> ACTIVE
          </span>
          <span v-if="app.status=={{ AppRental::STATUS_INACTIVE }}" class="label label-default">
            <i class="fa fa-power-off"></i> INACTIVE
          </span>
          <span v-if="app.status=={{ AppRental::STATUS_TERMINATED }}" class="label label-danger">
            <i class="fa fa-remove"></i> TERMINATED
          </span>
        </div>
        
        <div class="col-md-3 text-right">
          <br>
          <p class="text-muted text-sm m-none" 
            :tooltip="moment(app.finish_date).format('DD MMM YYYY')">
            @{{ format_date(app.finish_date) }} left
          </p>
        </div>
      </div>
    </div>

    <div class="dropdown dropdown-app">
      <a class="dropdown-toggle text-muted" data-toggle="dropdown">
        <i class="fa fa-caret-down"></i>
      </a>
      <ul class="dropdown-menu">
        <li>
          <a :href="'{{ url("?app=") }}' + app.code">
            <i class="fa fa-arrows-h"></i> Extends
          </a>
        </li>

        <li>
          <a :href="'{{ url("rental/status") }}/' + app.code" 
            v-if="app.status=={{ AppRental::STATUS_ACTIVE }}">
            <i class="fa fa-power-off"></i> Set Inactive
          </a>
          <a :href="'{{ url("rental/status") }}/' + app.code" 
            v-if="app.status=={{ AppRental::STATUS_INACTIVE }}">
            <i class="fa fa-check"></i> Set Active
          </a>
        </li>
      </ul>
    </div>
  </div>
@endsection

@push("script")
  <script type="text/javascript" src="{{ asset("js/lib/moment.min.js") }}"></script>
  <script type="text/javascript" src="{{ asset("js/pages/dashboard.js") }}"></script>
@endpush

@push("style")
  <style type="text/css">
    .row .input-group {
      display: table-cell;
    }
    .media {
      overflow: visible;
      position: relative;
    }
    .media .dropdown-app {
      position: absolute;
      top: 10px;
      right: 0px;
    }
    .media .dropdown-app .dropdown-toggle {
      padding: 10px 20px;
    }
    .dropdown-menu {
      right: 0px;
      left: initial;
    }
    .dropdown-toggle {
      cursor: pointer;
    }
  </style>
@endpush