<div class="panel panel-default">
	<div class="panel-body">
		<div class="nav nav-pills">
		@foreach ([
			"dashboard" => "<i class='fa fa-rocket'></i> Applications",
			"deposit" => "<i class='fa fa-money'></i> Deposit",
		] as $url => $label)
			<li @if (strpos(Request::path(), $url) !== false) class="active" @endif>
				<a href="{{ url($url) }}">{!! $label !!}</a>
			</li>	
		@endforeach
		</div>
	</div>
</div>