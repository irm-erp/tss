@extends("layout.app")

@section("content-layout")
  <header>
    <div class="container" id="maincontent" tabindex="-1">
      <div class="row">
        <div class="col-lg-12">
          <div class="intro-text">
            <i class="fa fa-rocket" style="font-size: 150px"></i>
            <br><br><hr class="star-light">
            <h1> Enterprise Resource <br> Planning </h1>

            <a href="#app-list" style="color: #fff; display: block">
              <br><br><i class="fa fa-chevron-down" style="font-size: 50px"></i>
            </a>
          </div>
        </div>
      </div>
    </div>
  </header>

  <section id="app-list">
    <h3 class="m-none text-center">@lang("app.application.welcome")</h3> 
    <br> <br> <br>

    <div class="container" id="maincontent" tabindex="-1">
      @foreach ($list as $item)
        <div class="box col-md-3 col-sm-3 col-xs-4">
          <a href="{{ route("app.show", $item->code) }}" modal-lg class="thumbnail">
            <img src="{{ asset($item->image) }}" class="p-xl" />

            <div class="caption bg-primary">
              <h6 class="text-center m-none bg-primary">{{ $item->name }}</h6>
            </div>
          </a>
        </div>
      @endforeach
    </div>
  </section>
@endsection

@push("style")
  <style type="text/css">
    #app-list .box:nth-child(4n+1) {
      clear: both;
    }
    #app-list .thumbnail .caption {
      /*height: 120px;*/
    }
    #app-list .thumbnail .caption p {
      font-size: 13px; 
      overflow: hidden;
      /*height: 50px; */

    }
    /*#app-list .thumbnail:hover img {
      padding: 0px !important;
      transition: ease 0.4s;
    }*/
  </style>
@endpush