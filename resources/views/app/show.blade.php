@extends("app")

@section("content")
  @section("content-ajax")
    <div class="row">
      <div class="col-md-3 col-sm-5">
        <div class="p-xl">
          <img alt="{{ $model->name }}" class="img-responsive"height="300"
            onerror='this.src="{{ asset("img/application/default.png") }}"' 
            src="{{ asset("img/application/$model->code.png") }}" >
        </div>
      </div>

      <div class="col-md-9 mt-md">
        <h1 class="mb-md"><b>{{ $model->name }}</b></h1> <hr>
        <p class="text-xlg">{!! $model->description !!}</p>
      </div>
    </div>
  @show

  <div class="text-right mt-xlg pt-xlg">
    <a href="{{ url("/") }}" class="btn btn-warning">
      <i class="fa fa-reply"></i> Kembali
    </a>
  </div>
@endsection