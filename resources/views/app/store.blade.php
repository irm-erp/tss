@extends("app")

@section("content")
  {{-- Store --}}
  <div v-if="!is_checkout">
    <div class="row">
      <div v-for="(app, i) in applications" class="box col-sm-4 col-xs-12 p-sm">
        {{-- Application --}}
        <div class="thumbnail m-none p-none">
          <a :href="fn.url('show/'+ app.code)" modal-lg class="title caption bg-primary">
            <b>@{{ app.name }}</b>
          </a>

          <div class="row">
            <div class="col-md-4 col-xs-4 pr-none">
              <img :src="fn.url(app.image)" class="p-xl" width="100%" />
            </div>
            
            <div class="col-md-8 col-xs-8">
              <p class="description text-sm text-muted p-sm">@{{ app.description }}</p>
              <p class="text-right text-sm mt-sm pr-md">
                <a><b>@{{ app.tarif | currency }} / Month</b></a>
              </p>
            </div>
          </div>
          
          <hr class="m-none">

          <div class="row p-sm text-muted">
            <span class="col-xs-8 text-sm pt-xs pr-none text-right">
              <i class="fa fa-shopping-basket"></i>
              <b>@{{ (app.qty > 0 ? app.tarif * app.qty : '') | currency }}</b>
            </span>
            <div class="col-xs-4">
              <input type="number" 
                class="form-control input-sm" 
                value="0" 
                min="0" 
                @change="setQty($event, i)"
                @keyup="setQty($event, i)" />
            </div>
          </div>
        </div>
      </div>
    </div>

    {{-- Footer Summary --}}
    <div id="footer-summary">
      <div class="container">
        <div class="pull-left mr-xlg">
          <b>
            <i class="fa fa-money"></i> Deposit 
            <a href="{{ url("deposit/topup") }}" 
              class="btn btn-xs btn-default" 
              modal tooltip="Top Up">
              <i class="fa fa-arrow-up"></i>
            </a>
          </b>
          <div class="text-sm text-muted mt-xs">
            <b>@{{ deposit | currency }}</b>
          </div>
        </div>

        <div class="pull-left mr-xlg">
          <b><i class="fa fa-shopping-basket"></i> Total</b>
          <div class="text-sm text-muted mt-xs">
            <b v-if="total > 0" :class="{'text-danger': !this.is_valid}">
              @{{ total | currency }}
            </b>
            <i v-else>(empty)</i>
          </div>
        </div>

        <button type="button" 
          class="btn btn-lg btn-primary pull-right" 
          :class="{disabled: !this.is_valid}" 
          @click="checkout()">
          <i class="fa fa-shopping-basket"></i> Checkout
        </button>
      </div>
    </div>
  </div>

  {{-- Checkout --}}
  <div v-else>
    <div class="row mb-md">
      <div class="col-xs-6"></div>
      <div class="col-xs-6 text-right">
        <i class="fa fa-money"></i> 
        <b>@{{ total | currency }}</b>
      </div>
    </div>

    {!! Form::open(["url" => "rental/request"]) !!}
      <table class="table table-hover table-striped table-bordered">
        <colgroup>
          <col width="50%" />
          <col width="20%" />
          <col width="10%" />
          <col width="20%" />
        </colgroup>
        <thead>
          <tr>
            <th>Application</th>
            <th>Tarif / Month</th>
            <th>Month Qty</th>
            <th>Sub Total</th>
          </tr>
        </thead>
        <tbody>
          <tr v-for="app in cart" v-if="app.qty > 0">
            <th>
              <img :src="fn.url(app.image)" height="50px" />
              <span>@{{ app.name }}</span>
            </th>
            <td>@{{ app.tarif | currency }}</td>
            <td>
              <input type="number" 
                :name="'app['+ app.code +']'" 
                class="form-control input-sm" 
                min="0" 
                v-model="app.qty" />
            </td>
            <td>@{{ (app.tarif * app.qty) | currency }}</td>
          </tr>
        </tbody>
        <tfoot>
          <tr>
            <th colspan="3" class="text-right">Total</th>
            <th>@{{ total | currency }}</th>
          </tr>
        </tfoot>
      </table>

      <br>
      <div class="text-right">
        <button type="button" class="btn btn-lg btn-default" @click="is_checkout = false">
          <i class="fa fa-reply"></i> Back
        </button>
        <button type="submit" class="btn btn-lg btn-primary" @click="validate($event)">
          <i class="fa fa-check"></i> Proceed
        </button>
      </div>
    {!! Form::close() !!}
  </div>
@endsection

@push("script")
  <script type="text/javascript">
    var deposit = {{ Tenant::user()->deposit }};
  </script>
  <script type="text/javascript" src="js/pages/store.js"></script>

  {{-- Initialization --}}
  <script type="text/javascript">
    axios.get("application.json")
      .then(function(res) {
        v.applications = res.data;
        
        {{-- App Extends --}}
        @if (Request::has("app"))
          var code = '{{ Request::input("app") }}';

          for (var i in v.applications) {
            if (v.applications[i].code == code) {
              v.applications[i].qty = 1;
            }
          }

          v.checkout();
        @endif
      });
  </script>
@endpush

@push("style")
  <style type="text/css">
    #content {
      min-height: 580px;
    }
    #content .box:nth-child(3n+1) {
      clear: both;
    }
    #content .box .title {
      overflow: hidden;
      white-space: nowrap;
      text-overflow: ellipsis;
    }
    #content .box .description {
      height: 100px;
      text-overflow: ellipsis;
      overflow: hidden;
      -webkit-line-clamp: 5;
      -webkit-box-orient: vertical;
      display: block;
      display: -webkit-box;
    }
    #content .thumbnail .caption {
      color: #fff;
      display: block;
    }
    #content .thumbnail .caption p {
      font-size: 13px; 
      overflow: hidden;
    }
    #footer-summary {
      position: fixed;
      z-index: 9999;
      width: 100%;
      left: 0px;
      bottom: 0px;
      padding: 10px;
      border-top: solid 1px #ccc;
      background: #fafafa;
      box-shadow: 0px 1px 5px #aaa;
    }
  </style>
@endpush