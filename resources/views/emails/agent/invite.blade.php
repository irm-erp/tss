@component('mail::message')
<center>
# Invitation
__{{ $tenant->name }} ({{ $tenant->fullname }})__ has sent you an invitation.
</center>

@component('mail::button', ['url' => url("invitation/$invitation->id/$invitation->token")])
Accept
@endcomponent

@endcomponent
