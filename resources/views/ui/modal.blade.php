<?php
/**
 * modal frame bootstrap
 * @param  string|array $attr attribute modal
 * @param  string $id
 * @param  string $class
 * @param  string $title
 * @param  string $body 	html
 * @param  string $footer 	html
 */

$attribute = null;

if (isset($attr)) {
    if (is_array($attr)) {
        foreach ($attr as $key => $val)
            $attribute .= " $key='$val'";
    }
    else $attribute = $attr;
}

?>

<div class="modal fade" {!! $attribute !!} {!! isset($id) ? "id='$id'" : null !!}>
    <div class="modal-dialog {{ $class or null }}">
        <div class="modal-content">
            <div class="modal-header">
                <button type='button' aria-label="Close" data-dismiss="modal" class="close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">{{ $title or null }}</h4>
            </div>
            
            <div class="modal-body">
                {!! $body or null !!}
            </div>

            @if (isset($footer))
	            <div class="modal-footer">
	                {!! $footer !!}
	            </div>
	        @endif
        </div>
    </div>
</div>