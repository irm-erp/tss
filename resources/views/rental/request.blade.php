@extends("app")

@section("title", trans("action.create")." | Tenant")

@section("content")
	{!! Form::open(["@submit" => "validate(\$event)"]) !!}
		<div class="panel panel-primary">
			<div class="panel-heading">
				<div class="row">
					<div class="col-sm-6">
						Select any applications which your bussiness need.
					</div>
					<div class="col-sm-6 text-right">
						Your deposit balance : <b>{{ currency($tenant->deposit) }}</b>
						<a href="{{ url("tss/deposit/topup") }}" 
							class="ml-md btn btn-sm btn-default">
							<i class="fa fa-arrow-up"></i> Top Up
						</a>
					</div>
				</div>
			</div>

			<table id="table-app" class="table table-bordered table-striped table-hover">
				<colgroup>
					<col width="45%"></col>
					<col width="20%"></col>
					<col width="15%"></col>
					<col width="20%"></col>
				</colgroup>
				<thead>
					<tr>
						<th>Application</th>
						<th>Monthly Price</th>
						<th>QTY</th>
						<th>Subtotal</th>
					</tr>
				</thead>
				<tbody>
					<tr v-for="(item, i) in apps">
						<th>@{{ item.name }}</th>
						<th class="text-right">@{{ item.tarif | currency }}</th>
						<th>
							<input type="hidden" :name="'app['+ item.code +'][tarif]'" 
								:value="item.tarif" />
							<input type="number" class="form-control" min="0"
								v-model.number="item.qty" 
								:name="'app['+ item.code +'][qty]'" />
						</th>
						<th class="text-right">
							@{{ (item.tarif * item.qty) | currency }}
						</th>
					</tr>
					<tr>
						<th>User Account</th>
						<th class="text-right">@{{ user_account.tarif | currency }}</th>
						<th>
							<input type="hidden" :name="'user_account[tarif]'" 
								:value="user_account.tarif" />
							<input type="number" class="form-control" min="0"
								v-model.number="user_account.qty" 
								:name="'user_account[qty]'" />
						</th>
						<th class="text-right">
							@{{ (user_account.tarif * user_account.qty) | currency }}
						</th>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<th class="text-right" colspan="3">Total</th>
						<th class="text-right" :class="{'text-danger': total > deposit}">
							Rp @{{ total | currency }}
						</th>
					</tr>
				</tfoot>
			</table>
		</div>

		<div class="text-right">
			<button type="submit" class="btn btn-primary">
				<i class="fa fa-shopping-basket"></i> Submit Order
			</button>
		</div>
	{!! Form::close() !!}
@endsection

@push("script")
	<script type="text/javascript" src="{{ asset("js/lib/vue.min.js") }}"></script>
@endpush

@push("app-script")
	<script type="text/javascript">
		var v = new Vue({
			el: "body > .container",
			data: {
				deposit: 0,
				apps: [],
				user_account: {
					tarif: 0,
					qty: 0,
				}
			},
			computed: {
				total: function() {
					var total = 0;

					for (var i in this.apps) {
						total += this.apps[i].tarif * this.apps[i].qty;
					}

					total += this.user_account.tarif * this.user_account.qty;

					return total;
				},
			},
			methods: {
				validate: function(e) {
					if (this.total > this.deposit) {
						fn.flash( 
							"Your deposit is not sufficient. \
							You can top up first or just continue your order. \
							We will activate later after your balance is sufficient.",
							"danger"
						);

						e.preventDefault();
					}
				},
				init: function() {
					this.apps = {!! json_encode($list) !!}
					this.deposit = {{ $tenant->deposit }};
					this.user_account.tarif = {{ $user_license_tarif }}
				},
			},
		});

		v.init();
	</script>
@endpush