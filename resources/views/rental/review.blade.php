@extends("app")

@section("title", "Review")

@section("content")
    <div class="text-right">
        <a href="{{ url("tss#orders") }}" class="btn btn-primary">
            <i class="fa fa-reply"></i> Kembali        
        </a>
    </div>
    <hr>

    <div class="form-group">
        @foreach ($fields as $field => $value)
            <div class="col-md-4">
                <label class="col-md-4 control-label text-right">
                    <b>{{ $model->label($field) }}</b>
                </label>
                <div class="col-md-8">{{ $value }}</div>
            </div>
        @endforeach
    </div>

    <div class="table-responsive">
        <table class="table table-hover table-bordered table-striped">
            <thead>
                <tr>
                    <th>{{ $model_item->label("app_code") }}</th>
                    <th>{{ $model_item->label("month_qty") }}</th>
                    <th>{{ $model_item->label("start_date") }}</th>
                    <th>{{ $model_item->label("finish_date") }}</th>
                    <th>{{ $model_item->label("sub_total") }}</th>
                </tr>
            </thead>
            <thead>
                <?php $total = 0; ?>

                @foreach (
                    $model_item
                        ->where("rental_id", $model->id)
                        ->join("application as a", "a.code", "=", "app_code")
                        ->orderBy("a.name") 
                        ->get() 
                    as $item)
                    <tr>
                        <td>{{ $item->name }}</td>
                        <td>{{ number_format($item->month_qty) }}</td>
                        <td>
                            {{ 
                                isset($item->start_date) 
                                    ? date("d F Y", strtotime($item->start_date)) : "-" 
                            }}
                        </td>
                        <td>
                            {{ 
                                isset($item->finish_date) 
                                    ? date("d F Y", strtotime($item->finish_date)) : "-" 
                            }}
                        </td>
                        <td class="text-right">{{ currency($item->sub_total) }}</td>
                    </tr>

                    <?php $total += $item->sub_total; ?>
                @endforeach
            </thead>
            <tfoot>
                <tr>
                    <th colspan="4" class="text-right">Total</th>
                    <th class="text-right">{{ currency($total) }}</th>
                </tr>
            </tfoot>
        </table>
    </div>
@endsection