@extends("app")

@section("title", "User License")

@section("content")
  {!! Form::open(["url" => "rental/user-license"]) !!}
    <div class="input-group">
      <input type="number" name="qty" placeholder="{{ currency($price) }} / user" 
        min="0" value="1" v-model="user_license" class="form-control" />
      <span class="input-group-btn">
        <button type="submit" class="btn btn-default">
          <i class="fa fa-shopping-basket"></i> Buy
        </button>
      </span>
    </div>

    @if (!Request::ajax())
      <div class="mt-sm">
        <b><i class="fa fa-dollar"></i> @{{ price | currency }}</b>
      </div>
    @endif
  {!! Form::close() !!}
@endsection

@push("script")
  <script type="text/javascript">
    var price = {{ $price }};

    var v = new Vue({
      el: "#content",
      data: {
        user_license: 1,
      },
      computed: {
        price: function() {
          return this.user_license * price;
        },
      }
    });
  </script>
@endpush
