@extends("tenant.app")

@section("title", "Profile")

@section("tenant.content")
    {!! Form::model($model) !!}
        <div class="panel panel-default">
            <div class="panel-body">
                {!! Form::group("text", "fullname") !!}
                {!! Form::group("text", "city") !!}
                {!! Form::group("textarea", "address") !!}
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">Commercial Contact</div>
            <div class="panel-body">
                {!! Form::group("text", "comm_contact_name") !!}
                {!! Form::group("text", "comm_contact_phone") !!}
                {!! Form::group("text", "comm_contact_mail") !!}
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-md-offset-3 col-md-9">
                {!! Form::submit(trans("action.save"), ["class" => "btn btn-primary"]) !!}
            </div>
        </div>
    {!! Form::close() !!}
@endsection