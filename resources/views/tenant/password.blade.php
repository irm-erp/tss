@extends("tenant.app")

@section("title", "Change Password")

@section("tenant.content")
	{!! Form::open() !!}
		{!! Form::group("password", "current_password") !!}
		{!! Form::group("password", "new_password") !!}
		{!! Form::group("password", "confirm_password") !!}

		<div class="form-group">
			<div class="col-md-offset-3 col-md-9">
				<button type="submit" class="btn btn-primary">@lang("action.save")</button>
			</div>
		</div>
	{!! Form::close() !!}
@endsection