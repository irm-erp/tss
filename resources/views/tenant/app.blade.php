@extends("app")

<?php

	Menu::make("tenant", function($menu) {
		$menu->add("Profile", "settings", "fa fa-user");
		$menu->add("Password", "settings/password", "fa fa-lock");
	});

?>

@section("content")
	<div class="row">
		<div class="col-md-6">
			<h3 class="m-none mt-xs">@yield("title")</h3>
		</div>
		<div class="col-md-6 text-right">
			@yield("header-content")
		</div>
	</div>
	<hr class="mt-sm">

	<div class="row">
		<div class="col-md-3">
			{!! 
				Menu::get("tenant")
					->setMenuView(function($menu) {
						return "<ul class='list-group'>$menu</ul>";
					})
					->setTreeView(function($menu, $attr) {
						$active = $attr["active"] ? "active" : "";

						return "<a href='".url($menu->url)."' class='list-group-item $active'>
								<i class='".$menu->getAttr("icon")."'></i> $menu->label
							</a>";
					})
					->render() 
			!!}
		</div>
		<div class="col-md-9">
			@yield("tenant.content")
		</div>	
	</div>
@endsection