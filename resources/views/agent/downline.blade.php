<div class="form-group row">
	<div class="col-xs-offset-6 col-xs-6">
		{!! Form::open(["url" => "agent/invite"]) !!}
			<div class="input-group col-xs-offset-4 col-xs-8">
				{!! Form::text("email", null, ["placeholder" => "Email"]) !!}
				
				<div class="input-group-btn">
					<button type="submit" class="btn btn-primary">
						<i class="fa fa-envelope"></i> Invite
					</button>
				</div>				
			</div>
		{!! Form::close() !!}
	</div>
</div>
<hr>

<div class="table-responsive">
	<table class="table table-bordered" datatable="{{ url("agent/downlines") }}">
	  <thead>
	    <tr>
	      <th dt-field="fullname"> Name </th>
	      <th dt-field="u.name"> Username </th>
	      <th dt-field="email"> Email </th>
	      {{-- <th dt-field="unclaimable_deposit"> Unclaimable Deposit </th> --}}
	    </tr>
	  </thead>
	</table>
</div>
