<?php

return [
	"rental" => [
	    "request" => [
	        "sent" => "Permintaan anda telah dikirim.",
	    ],
	    "validation" => [
	        "empty" => "Setidaknya pilih salah satu aplikasi untuk disewa.",
	    ],
		"user_license" => [
			"bought" => "User License ditambahkan",
		],
	],
];