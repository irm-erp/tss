window.axios.defaults.headers.common['X-CSRF-TOKEN'] = fn.csrf;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

Vue.filter("currency", function(value, decimals, placeholder) {
    return value=="" ? (placeholder || "") : fn.format.num(fn.int(value), decimals || 0);
});

fn.lang = function(query) {
	var result = query;

	$.ajax({
		async: false,
    cache: true,
		url: fn.url("lang.json"),
		data: {q: query},
		success: function(res) {
			result = res.data;
		},
	});

	return result;
}
