<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("dep_transaction", function($table) {
            $table->increments("id");
            $table->date("trans_date");
            $table->char("trans_mode", 1)->nullable();
            $table->boolean("is_debet");
            $table->string("ref_no", 20);
            $table->string("description")->nullable();
            $table->decimal("balance", 15, 2);

            $table->char("tenant_code", 8);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("dep_transaction");
    }
}
