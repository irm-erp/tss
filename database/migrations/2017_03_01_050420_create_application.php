<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplication extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("application", function($table) {
            $table->string("code", 5)->primary();
            $table->string("name", 50);
            $table->string("description")->nullable();
            $table->decimal("tarif", 15, 2);

            // $table->string("name_id", 50);
            // $table->string("name_en", 50);
            // $table->string("desc_id", 50);
            // $table->string("desc_en", 50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("application");
    }
}
