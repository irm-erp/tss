<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeatures extends Migration
{
    /**
     * Run the migrations.
     * @return  void
     */
    public function up()
    {
        Schema::create("features", function(Blueprint $table) {
            $table->string("code", 10)->primary();
            $table->string("name", 50);
            $table->string("description")->nullable();

            $table->string("app_code", 4);
        });
    }

    /**
     * Reverse the migrations.
     * @return  void
     */
    public function down()
    {
        Schema::drop("features");
    }
}
