<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenantInvitation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("tenant_invitation", function($table) {
            $table->increments("id");
            $table->string("tenant_code", 8);
            $table->string("receiver_mail");
            $table->string("token");
            $table->boolean("is_accepted")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("tenant_invitation");
    }
}
