<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRental extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("rental", function($table) {
            $table->increments("id");
            $table->string("contract_no", 20);
            $table->date("req_date");
            $table->integer("qty");
            $table->decimal("total", 15, 2);
            $table->char("status", 1);
            // $table->integer("user_license")->nullable();
            // $table->string("note")->nullable();

            $table->char("tenant_code", 8);
            $table->string("app_code", 5)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("rental");
    }
}
