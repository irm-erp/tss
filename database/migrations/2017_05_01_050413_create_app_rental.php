<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppRental extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("app_rental", function($table) {
            $table->increments("id");
            $table->date("start_date")->nullable();
            $table->date("finish_date")->nullable();
            $table->char("status", 1);

            $table->char("tenant_code", 8);
            $table->string("app_code", 5);

            $table->unique(["tenant_code", "app_code"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("app_rental");
    }
}
