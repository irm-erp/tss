<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("tenant", function($table) {
            $table->string("code", 8)->primary();
            $table->string("name", 16);
            $table->date("join_date");
            $table->string("fullname", 50)->nullable();
            $table->string("address")->nullable();
            $table->string("city", 50)->nullable();
            $table->string("tech_contact_name", 50)->nullable();
            $table->string("tech_contact_phone", 20)->nullable();
            $table->string("tech_contact_mail", 50)->nullable();
            $table->string("comm_contact_name", 50)->nullable();
            $table->string("comm_contact_phone", 20)->nullable();
            $table->string("comm_contact_mail", 50)->nullable();
            $table->decimal("claimable_deposit", 15, 2)->default(0);
            $table->decimal("unclaimable_deposit", 15, 2)->default(0);
            $table->integer("user_license")->nullable();

            $table->string("parent_code", 8)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("tenant");
    }
}
