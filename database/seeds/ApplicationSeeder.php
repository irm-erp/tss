<?php

use Illuminate\Database\Seeder;

class ApplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
			["AM", "Asset Management", "Buku bantu untuk memperinci berbagai catatan terkait dengan aktiva tetap perusahaan, termasuk berbagai aspek pengelolaannya seperti asuransi, pemeliharaan", 500000],
			["AP", "Account Payable", "Buku bantu untuk memperinci berbagai catatan terkait dengan hutang perusahaan", 400000],
			["AR", "Account Receivable", "Buku bantu untuk memperinci berbagai catatan terkait dengan piutang perusahaan", 300000],
			["CB", "Cash & Bank", "Pengelolaan berbagai rekening kas (baik kas penerimaan pembayaran maupun kas pengeluaran), valuta asing, rekening bank, serta berbagai jenis alat pembayaran sebagai fasilitas kepemilikan rekening bank tersebut (buku cek, bilyet giro, dll)", 200000],
			["EI", "Export Import Management", "Pengelolaan ekspor-impor barang dan kepabeanan", 167000],
			["GL", "Accounting", "Pembukuan dasar perusahaan, meliputi penyusunan chart of account, jurnal transaksi, periode akuntansi, saldo awal, hingga penyiapan berbagai jenis pelaporan standar akuntansi keuangan", 100000],
			["HC", "HR Development", "Pengembangan pegawai meliputi pelatihan dan penilaian kinerja", 550000],
			["IM", "Investment Management", "Pengelolaan investasi", 1000000],
			["LL", "Law & Legal Management", "Pengelolaan berbagai dokumen legal dan kolaborasi proses hukum", 1500000],
			["MB", "Microbanking", "Pengelolaan layanan tabungan / dana masyarakat", 900000],
			["MC", "Manufacturing - Continuous", "Proses produksi barang yang bersifat kontinu, seperti : tekstil (spinning, weaving, DF), kertas, plastik, produk curah, dsb", 375000],
			["MF", "Microfinance", "Pengelolaan kredit mikro", 850000],
			["MI", "Manufacturing - Intermitten", "Proses produksi barang yang bersifat diskrit / intermitten, seperti : fashion, perakitan elektronik, perakitan mesin, dsb", 425000],
			["OM", "Organization Management", "Penugasan pegawai dalam organisasi, mutasi, promosi, demosi, dsb", 450000],
			["PA", "Personel Administration", "Administrasi data induk kepegawaian, meliputi : biodata lengkap pegawai, keluarga, riwayat pendidikan, kontrak kerja, masa kerja, dsb", 250000],
			["PU", "Purchasing", "Pengadaan barang atau jasa melalui pemesanan kepada supplier atau pembelian langsung ", 115000],
			["PY", "Payroll", "Penggajian pegawai", 350000],
			["SO", "Sales Order", "Penjualan barang kepada customer berdasarkan kontrak penjualan atau surat pesanan", 145000],
			["TT", "HR Time Tracking", "Pengelolaan waktu kerja pegawai meliputi jadwal kerja, shift harian, kehadiran / attendance. Termasuk juga pengelolaan cuti", 275000],
			["TX", "Taxation", "Perpajakan dalam negeri, meliputi PPN, PBB, dan PPh23", 780000],
			["WH", "Warehouse Management", "Pengelolaan gudang-gudang milik perusahaan, meliputi pendataan berbagai atribut fisik (dimensi, layout, rak, slot penyimpanan), penempatan barang, barcoding, barang persediaan / inventory, dsb", 600000],
			["00", "User License", "User License", 75000],
        ];

        $insert = [];

        foreach ($data as $item) {
        	$insert[] = [
        		"code" => current($item),
        		"name" => next($item),
        		"description" => next($item),
        		"tarif" => next($item),
        	];
        }

        DB::table("application")->insert($insert);
    }
}
