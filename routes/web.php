<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::auth();

// Application List
Route::get("/", "AppController@index");
Route::get("show/{id}", "AppController@show")->name("app.show");

// Storage Files
Route::get("file", "StorageController@file");
Route::get("image", "StorageController@image");

// Account Verification
Route::get("verify/{tenant_code}/{code}", "TenantController@verify");

// Accept Invitation
Route::get("invitation/{invitation_id}/{token}", "TenantController@acceptInvitation");

// Tenant
Route::group(["middleware" => ["auth"]], function() {
    // Dashboard
    Route::get("dashboard", "AppController@dashboard");

    // Application Rental
    Route::group(["prefix" => "rental"], function() {
        Route::post("request", "RentalController@request");
        Route::match(["get", "post"], "user-license", "RentalController@userLicense");
        Route::get("status/{code}", "RentalController@status");
    });

    // Deposit
    Route::group(["prefix" => "deposit"], function() {
        Route::get("/", "DepositController@index");
        Route::any("history", "DepositController@history");
        Route::match(["get", "post"], "topup", "DepositController@topup");
    });

    // Agent
    Route::group(["prefix" => "agent"], function() {
        Route::any("downlines", "AgentController@downlines");
        Route::post("invite", "AgentController@invite");
    });

    // Account Setting
    Route::group(["prefix" => "settings"], function() {
        Route::match(["get", "post"], "/", "TenantController@profile");
        Route::match(["get", "post"], "password", "TenantController@password");
    });
});

// Json Provider
Route::get("{data}.json", "JsonController@handle");
