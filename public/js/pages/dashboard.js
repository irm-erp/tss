// VueJS
var v = new Vue({
	el: "#content",
	data: {
		applications: [],
	},
	methods: {
		format_date: function(value) {
			var breakdown = ["years", "months", "weeks", "days"];
			var date = moment(value);
			var format;

			for (var i in breakdown) {
				var breakdown_val = date.diff(moment(), breakdown[i]);

				if (breakdown_val > 0)
					return breakdown_val +" "+ breakdown[i];
			}
		},
	},
	computed: {
		app_count: function() {
			var count = {
				installed: this.applications.length,
				active: 0,
				inactive: 0,
				terminated: 0,
			};

			for (var i in this.applications) {
				switch (this.applications[i].status) {
					case "0": count.inactive++; break;
					case "1": count.active++; break;
					case "2": count.terminated++; break;
				}
			}

			return count;
		},
	}
});

/** Initialization */
axios.get("app-rental.json")
  .then(function(res) {
    v.applications = res.data;
    fn.event.init(/*$("#content")*/);
  });