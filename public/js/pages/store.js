// VueJS
var v = new Vue({
	el: "#content",
	data: {
		deposit: deposit,
		is_checkout: false,
		applications: [],
	},
	methods: {
		checkout: function() {
			this.validate();
			this.is_checkout = this.is_valid;
		},
		validate: function(e) {
			if (!this.is_valid) {
				fn.flash("Your deposit is unsufficient.", "danger");

				if (!fn.empty(e)) {
					e.preventDefault();
				}
			}
		},
		setQty: function(e, i) {
			this.$set(this.applications[i], "qty", e.target.value);
		},
	},
	computed: {
		is_valid: {
			cache: false,
			get: function() {
				return this.total > 0 && this.deposit >= this.total;
			},
		},
		cart: function() {
			var cart = [];

			for (var i in this.applications) {
				if (this.applications[i].qty <= 0)
					continue;

				cart.push(this.applications[i]);
			}

			return cart;
		},
		total: {
			cache: false,
			get: function() {
				var total = 0;

				for (var i in this.cart) {
					var sub_total = this.cart[i].qty*this.applications[i].tarif;

					if (fn.isNumeric(sub_total))
						total += sub_total;
				}

				return total;
			}
		}
	}
});